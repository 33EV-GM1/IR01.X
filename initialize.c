/*! \file  initialize.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 8:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/LCD.h"
#include "../include/basic33E.h"

/*! initialize - Initialize ports, clock and LCD */

/*!
 *
 */
void initialize(void)
{
  /* Initialize the processor clock */
  initClock();
  
  /* Initialize the A/D converter */
  ANSELAbits.ANSA0 = 0; /* Make RA0 digital */
  
  /* Initialize the timer */
  TMR5 = 0;
  PR5 = 0xfffe;
  T5CONbits.TCKPS = 2;
  T5CONbits.TON = 1;

  /* Initialize the LCD */
  LCDinit();
}
