/*! \file  snore.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 8:03 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! snore - Waste specified number of milliseconds */

/*!
 *
 */
void snore( int howmuch )
{
  /* Initialize the timer */
  TMR2 = 0;
  T2CONbits.TCKPS = 2;
  T2CONbits.TON = 1;
  while ( howmuch )
    {
      /* Calc says 875, but this seems to give a closer result */
      while(TMR2<1030)
        ;
      TMR2=0;
      howmuch--;
    }

}
