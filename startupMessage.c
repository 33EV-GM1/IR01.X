/*! \file  startupMessage.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 8:21 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "../include/LCD.h"
#include "IR01.h"

/*! startupMessage - Display a startup message*/

/*!
 *
 */
void startupMessage(void)
{
  int i;
  for (i = 0; i < 5; i++)
    {
      /* Startup banner */
      LCDclear();
      LCDputs(" Test Infrared  ");
      LCDline2();
      LCDputs("     Remote     ");
      snore(1000);
      /* Compilation date and time */
      LCDclear();
      LCDputs("  ");
      LCDputs(__DATE__);
      LCDline2();
      LCDputs("   ");
      LCDputs(__TIME__);
      snore(1000);
    }
}
