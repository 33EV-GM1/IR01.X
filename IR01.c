/*! \file  IR01.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 9:46 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "../include/LCD.h"
#include "../include/configurationFuses.h"
#include "IR01.h"

/*! main - */

/*!
 *
 */
int main(void)
{
  long i,j;
  int done;
  unsigned int pulses[128];
  char szWork[64];
  long value;

  initialize();
  startupMessage();

  while (1)
    {
      /* Prompt for key press */
      LCDclear();
      LCDputs("   Press key    ");
      j = 0;
      
      /* Wait for initial pulse */
      while(_RA0)
        ;
      TMR5 = 0;
      LCDclear();
      while ( !_RA0 )
        ;
      pulses[1] = TMR5;
      while (_RA0)
        ;
      pulses[2] = TMR5;
      
      /* Now collect pulse lengths */
      for (i = 0; i < 70; i+=2)
        {
          while (!_RA0)
            ;
          pulses[i] = TMR5;
          if ( (i>30) && (TMR5>2200) )
            break;
          TMR5 = 0;
          while (_RA0)
            ;
          pulses[i+1] = TMR5;
          if ( (i>30) && (TMR5>2200) )
            break;
          TMR5 = 0;
        }
      
      /* Add up the pulses as bits */
      value = 0;
      for ( i=19; i<70; i++ )
        {
          done=0;
          if ( !(i & 1) )
            {
              if ( (pulses[i]>500) && (pulses[i]<620) ) 
                done=1;
              //LCDline2();
            }
          else
            //LCDhome();
          if ( !done )
            {
              if ( i>30 )
                {
                  value *= 2; /* Shift left one bit */
                  if ( pulses[i] > 1000 )
                    value++;
                }
              //sprintf(szWork,"%3ld %5u  ",i,pulses[i]);
              //LCDputs(szWork);
              if ( pulses[i]>2200 )
                break;
              //snore(100);
            }
        }
      
      /* Show the results */
      sprintf(szWork,"0x%08lx  ",value);
      LCDclear();
      LCDputs(szWork);
      sprintf(szWork,"%08ld  ",value);
      LCDline2();
      LCDputs(szWork);
      strcpy(szWork," ");
      szWork[0]=codeToKey(value);
      LCDposition(0x0d);
      LCDputs("Key");
      LCDposition(0x4e);
      LCDputs(szWork);
      snore(2000);
    }

  return 0;
}
