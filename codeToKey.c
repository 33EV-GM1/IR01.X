/*! \file  codeToKey.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 8:04 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! codeToKey - Convert pulse value to the letter on the key */

/*!
 *
 */
unsigned char codeToKey(long value)
{
  unsigned char ch;

  ch = ' ';
  switch (value)
    {
      case 148923:
        ch = 0x7f;
        break;
      case 230523:
        ch = 0x7e;
        break;
      case 181563:
        ch = '^';
        break;
      case 217263:
        ch = 'V';
        break;
      case 165243:
        ch = '*';
        break;
      case 173403:
        ch = '#';
        break;
      case 132603:
        ch = 0xdb;
        break;
      case 184623:
        ch = '1';
        break;
      case 209103:
        ch = '2';
        break;
      case 221343:
        ch = '3';
        break;
      case 156063:
        ch = '4';
        break;
      case 143823:
        ch = '5';
        break;
      case 193803:
        ch = '6';
        break;
      case 139743:
        ch = '7';
        break;
      case 160143:
        ch = '8';
        break;
      case 177483:
        ch = '9';
        break;
      case 169323:
        ch = '0';
        break;
    }
  return ch;
}
