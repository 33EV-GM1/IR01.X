/*! \file  IR01.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 15, 2017, 8:00 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef IR01_H
#define	IR01_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! initialize - Initialize ports, clock and LCD */
void initialize(void);

/*! snore - Waste specified number of milliseconds */
void snore(int);

/*! codeToKey - Convert pulse value to the letter on the key */
unsigned char codeToKey(long);

/*! startupMessage - Display a startup message*/
void startupMessage(void);

#ifdef	__cplusplus
}
#endif

#endif	/* IR01_H */

